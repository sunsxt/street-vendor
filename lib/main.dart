import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:streetvendor/app.dart';
import 'package:streetvendor/utils/colors.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: primaryDark
  ));
  runApp(App());
}
